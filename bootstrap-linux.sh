#!/bin/sh

uid=$(id -u)
if [ $uid -eq 0 ]; then
    # Basic requirements
    apt-get -y install bash ansible sudo git wicd
    echo "$SUDO_USER ALL=(ALL:ALL) NOPASSWD: ALL" >/etc/sudoers.d/$SUDO_USER
else
    if [ -x /usr/bin/ansible -a -x /usr/bin/git ]; then
        git clone https://framagit.org/jqueuniet/bootstrap-linux-desktop.git
        cd bootstrap-linux-desktop
        ansible-playbook -i inventory/localhost.ini workstation.yaml
    else
        echo "Please run this script as root first to install dependencies"
    fi
fi


